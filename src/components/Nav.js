import React, {useContext, useState, useEffect} from 'react'
import {Navbar,Nav, Modal, Button} from 'react-bootstrap'
import {Link,NavLink} from 'react-router-dom'
import UserContext from '../userContext'
import logo from '../assets/sycamore.png'
import Swal from 'sweetalert2'

export default function NavBar() {

  const {user} = useContext(UserContext)

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [isActive, setIsActive] = useState(true)

  const [categoryName, setCategoryName] = useState('')
  const [categoryType, setCategoryType] = useState('')
  
  useEffect(()=>{
    if(categoryName!==''&&categoryType!==''){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  },[categoryName,categoryType])

  // console.log(categoryName);
  // console.log(categoryType);

  // let token = localStorage.getItem('token')
  // console.log(`Bearer ${token}`)

  function addCategory() {

    

    fetch('https://dry-shelf-26448.herokuapp.com/api/categories/',{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name: categoryName,
        type: categoryType
      })
    })
    .then(res=> res.json())
    .then(data=>{
      console.log(data);
      if(data.error){
        Swal.fire({
          icon: "error",
          title: "Failed!",
          text: data.error
        })
      } else {
        Swal.fire({
          icon: "success",
          title: "Success!",
          text: data.message
        })
        setCategoryName('')
        setCategoryType('')

      }
    })
    
  }

  function submitAndClose(){
    addCategory()
    handleClose()
  }

  return (
      <>
        <Navbar variant="dark" expand="lg" className="navbar">
          <Navbar.Brand className="brand" as={Link} to="/">
            <img className="logo " src={logo} alt=""/>
             Seekamore</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav" className="nav-links">
            <Nav className="ml-auto">
              {
                user.email
                ?
                <div className="ml-auto navitems">
                <Nav.Link as={NavLink} to="/tracker">My Tracker</Nav.Link>
                <Nav.Link as={NavLink} to="/tracker" onClick={handleShow}>Add Category</Nav.Link>
                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                </div>
                :
                <div className="ml-auto navitems">
                  <Nav.Link as={NavLink} to="/signup">Signup</Nav.Link>
                  <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                </div>
              }
            </Nav>
          </Navbar.Collapse>
        </Navbar>


        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Add new Category</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <label htmlFor="categoryName">Name: </label><br/>
            <input placeholder="Example. Water Bill" className="w-100" type="text" value={categoryName} onChange={e => {setCategoryName(e.target.value)}}/>
            <br/>
            <br/>

            <label htmlFor="categoryType">Type:</label><br/>
            <select className="categoryType w-100" name="categoryType" value={categoryType} onChange={e => setCategoryType(e.target.value)}>
              <option value="" disabled selected>Choose Category Type</option>
                <option value="income">Income</option>
                <option value="expense">Expense</option>
            </select>
            <br/>
            <br/>
            {
            isActive
            ?
            <Button variant="primary" onClick={submitAndClose}>
              Add Category
            </Button>
            :
            <Button variant="primary" disabled>
              Add Category
            </Button>
          }
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            
          </Modal.Footer>
        </Modal>
      </>

  );
}