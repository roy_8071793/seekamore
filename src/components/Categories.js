import React,{useState,useEffect} from 'react'
import {Row,Col,Card,Button,Modal} from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPen,faTrashAlt } from '@fortawesome/free-solid-svg-icons'
// import incCategories from './AddTransaction'
// import expCategories from './AddTransaction'


export default function Categories(props){
	// console.log(props)

	const edit = <FontAwesomeIcon icon={faPen} />
	const del = <FontAwesomeIcon icon={faTrashAlt} />

	const [incomeCategories, setIncomeCategories] = useState([])
	const [expenseCategories, setExpenseCategories] = useState([])

	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	// const [isActive, setIsActive] = useState(true)

	const [categoryName, setCategoryName] = useState('')
 	const [categoryType, setCategoryType] = useState('')
 	const [categoryId,setCategoryId] = useState('')
 	// const [name,setname] = useState('')
 	// const [type,setType] = useState('')

 	// console.log(incCategories);
 	// console.log(expCategories);

 	

	useEffect(()=>{

		fetch('https://dry-shelf-26448.herokuapp.com/api/categories/',{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			// console.log(data);
			// setAllCategories(data)
			setExpenseCategories(data.filter(category=>category.type==='expense')
				.map(category=>{
					return(
						<Card key={category._id} className="w-50 mx-auto text-center card">
						  <span id={category._id}></span>
						  <Card.Header>{category.name}</Card.Header>
						  <Card.Text>
						      {category.type}
						   </Card.Text>
						  <Card.Footer className="text-muted">
						  	<Button variant="outline-info category-btn" id={category._id} onClick={showAndGetCategory}>{edit}</Button>
						  	<Button variant="outline-danger category-btn" id={category._id} onClick={deleteCategory}>{del}</Button>
						  </Card.Footer>
						</Card>
					)
				}))
			setIncomeCategories(data.filter(category=>category.type==='income')
				.map(category=>{
					return(
						<Card key={category._id} className="w-50 mx-auto text-center card">
						  <span id={category._id}></span>
						  <Card.Header>{category.name}</Card.Header>
						  <Card.Text>
						      {category.type}
						   </Card.Text>
						  <Card.Footer className="text-muted">
						  	<Button variant="outline-info category-btn" id={category._id} onClick={showAndGetCategory}>{edit}</Button>
						  	<Button variant="outline-danger category-btn" id={category._id} onClick={deleteCategory}>{del}</Button>
						  </Card.Footer>
						</Card>
					)
				}))
			
		})
		// .then(window.location.reload())
		
	//has a bug here-infi loop
	// },[])
	},[del,edit,showAndGetCategory])

	function deleteCategory(e){
		// console.log(e.target.id);
 	// 	// console.log(incomeCategories);
 	// 	incomeCategories.filter(category=>category.id===e.target.id)
 		fetch(`https://dry-shelf-26448.herokuapp.com/api/categories/${e.target.id}`,{
 			method: 'DELETE',
 			headers: {
 				Authorization: `Bearer ${localStorage.getItem('token')}`
 			}
 		})
 		.then(res=>res.json())
 		.then(data=>{
 			console.log(data);
 		})

 	}

 	function updateCategory(){
		fetch(`https://dry-shelf-26448.herokuapp.com/api/categories/${categoryId}`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: categoryName,
				type: categoryType
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})
 	}


 	function showAndGetCategory(e){
 		// handleShow()
 		// console.log("cliked");
 		fetch(`https://dry-shelf-26448.herokuapp.com/api/categories/${e.target.id}`,{
 			headers: {
 				Authorization: `Bearer ${localStorage.getItem('token')}`
 			}
 		})
 		.then(res=>res.json())
 		.then(data=>{
 			console.log(data);
 			setCategoryId(data._id)
 			setCategoryName(data.name)
 			setCategoryType(data.type)
 		})
 		.then(handleShow())
 	}

 	function closeandUpdate(){
 		updateCategory()
 		handleClose()
 	}


	
	

	return (
		<>
			<div className="sectionContainer">
				<h2 className="section">Categories</h2>
				<Row>
					<Col>
						<h3>Income</h3>
						<div className="incomeCategories">
							{incomeCategories}
						</div>
					</Col>
					<Col>
						<h3>Expense</h3>
						<div className="expenseCategories">
							{expenseCategories}
						</div>
					</Col>
				</Row>
			</div>

			<Modal show={show} onHide={handleClose}>
	          <Modal.Header closeButton>
	            <Modal.Title>Update Category</Modal.Title>
	          </Modal.Header>
	          <Modal.Body>
	            <label htmlFor="categoryName">Name: </label><br/>
	            <input placeholder="Example. Water Bill" className="w-100" type="text" value={categoryName} onChange={e => {setCategoryName(e.target.value)}}/>
	            <br/>
	            <br/>

	            <label htmlFor="categoryType">Type:</label><br/>
	            <select className="categoryType w-100" name="categoryType" value={categoryType} onChange={e => setCategoryType(e.target.value)}>
	              <option value="" disabled selected>Choose Category Type</option>
	                <option value="income">Income</option>
	                <option value="expense">Expense</option>
	            </select>
	            <br/>
	            <br/>
	            <Button variant="primary" onClick={closeandUpdate}>
		              Update Category
		        </Button>
	          </Modal.Body>
	          <Modal.Footer>
	            <Button variant="secondary" onClick={handleClose}>
	              Close
	            </Button>
	            
	          </Modal.Footer>
	        </Modal>
		</>


		
	)
}