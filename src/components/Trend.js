import React from 'react'
import MonthlyTrend from './MonthlyTrend'

export default function Trend() {
	return(
		<div className="sectionContainer">
			<h2>Trend</h2>
	        <div className="entriesOuter">
	        	<div className="entriesInner">
	        		<MonthlyTrend />
	        	</div>
	        </div>
		</div>
	)
}