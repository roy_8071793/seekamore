import React, {useContext,useEffect} from 'react'
import UserContext from '../userContext'
import {Redirect} from 'react-router-dom'

export default function Logout(){
	const {setUser,unsetUser} = useContext(UserContext)

 	useEffect(()=>{
 		unsetUser()
 		setUser({
	 		email: null,
	 		isAdmin: null
	 	})
 	})

	return (
		
		<Redirect to="/login"/>

	)
}