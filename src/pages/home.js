import React, {useContext} from 'react'
import {Jumbotron, Button, Container} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import UserContext from '../userContext'

export default function Home(){
	const {user} = useContext(UserContext)
	return(
		<Container>
			<Jumbotron>
			  <h1>Welcome to Seekamore!</h1>
			  <br/>
			  <br/>
			  <h3>
			    Track and balance your cashflow with seekamore.
			  </h3>
			  <br/>
			  <p>
			  	A simple web app where you can log your income, day-to-day expenses, monthly bills and other miscellaneous expenses.
			  </p>
			  <br/>
			  	{
			  		user.email
			  		?
			  		<Button className="jumbotron-btn" variant="dark" as={Link} to="/tracker">Go to your tracker!</Button>
			  		:
			  		<Button className="jumbotron-btn" variant="dark" as={Link} to="/signup">Sign up now!</Button>
			  	}
			    
			</Jumbotron>
		</Container>
	)
}